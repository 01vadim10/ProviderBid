﻿CREATE TABLE [dbo].[Tariff] (
    [TariffID] INT            IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([TariffID] ASC)
);

