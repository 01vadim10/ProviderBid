﻿SET IDENTITY_INSERT [dbo].[Bids] ON
INSERT INTO [dbo].[Bids] ([BidID], [FirstName], [LastName], [MidName], [Address], [TariffName]) VALUES (1, N'Alex', N'Kochetkov', N'Olegovich', N'Sunshine st.', N'Medium')
INSERT INTO [dbo].[Bids] ([BidID], [FirstName], [LastName], [MidName], [Address], [TariffName]) VALUES (2, N'Test', N'Testov', N'Testovich', N'Testing st.', N'Light')
INSERT INTO [dbo].[Bids] ([BidID], [FirstName], [LastName], [MidName], [Address], [TariffName]) VALUES (3, N'Green', N'Red', N'Blue', N'Coloring st', N'Medium')
INSERT INTO [dbo].[Bids] ([BidID], [FirstName], [LastName], [MidName], [Address], [TariffName]) VALUES (4, N'First', N'Num', N'Zerovich', N'Sum st.', N'Medium')
SET IDENTITY_INSERT [dbo].[Bids] OFF
