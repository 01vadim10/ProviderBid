﻿using System.Linq;
using ProviderBid.Domain.Entities;

namespace ProviderBid.Domain.Abstract
{
    public interface IBidRepository
    {
        IQueryable<Bid> Bids { get;}
        void SaveBid(Bid bid);
        Bid DeleteBid(int bidId);
    }
}
