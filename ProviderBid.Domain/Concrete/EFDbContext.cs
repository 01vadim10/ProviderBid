﻿using System.Data.Entity;
using ProviderBid.Domain.Entities;

namespace ProviderBid.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public DbSet<Bid> Bids { get; set; }
    }
}
