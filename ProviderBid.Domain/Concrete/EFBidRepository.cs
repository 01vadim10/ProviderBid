﻿using System.Data.Entity;
using System.Linq;
using ProviderBid.Domain.Abstract;
using ProviderBid.Domain.Entities;

namespace ProviderBid.Domain.Concrete
{
    public class EFBidRepository : IBidRepository
    {
        private EFDbContext _context = new EFDbContext();

        public IQueryable<Bid> Bids
        {
            get { return _context.Bids; }
        }

        public void SaveBid(Bid bid)
        {
            if (bid.BidID == 0)
            {
                _context.Bids.Add(bid);
            }
            else
            {
                var dbEntry = _context.Bids.Find(bid.BidID);
                if (dbEntry != null)
                {
                    dbEntry.FirstName = bid.FirstName;
                    dbEntry.LastName = bid.LastName;
                    dbEntry.MidName = bid.MidName;
                    dbEntry.Address = bid.Address;
                    dbEntry.TariffName = bid.TariffName;
                    dbEntry.BidID = bid.BidID;
                }
            }
            _context.SaveChanges();
        }

        public Bid DeleteBid(int bidId)
        {
            var dbEntry = _context.Bids.Find(bidId);
            if (dbEntry == null) return null;
            _context.Bids.Remove(dbEntry);
            _context.SaveChanges();
            return dbEntry;
        }
    }
}
