﻿namespace ProviderBid.Domain.Entities
{
    public enum Tariff
    {
        Light,
        Medium,
        FastAndHeavy,
        Infinity
    }
}
