﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ProviderBid.Domain.Entities
{
    public class Bid
    {
        [HiddenInput(DisplayValue = false)]
        public int BidID { get; set; }

        [Required(ErrorMessage = "Пожалуйста добавьте Имя")]
        [DataType(DataType.Text)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Пожалуйста добавьте Фамилию")]
        [DataType(DataType.Text)]
        public string LastName { get; set; }
        [DataType(DataType.Text)]

        [Required(ErrorMessage = "Пожалуйста добавьте Отчество")]
        public string MidName { get; set; }

        [Required(ErrorMessage = "Пожалуйста добавьте Адрес")]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [Required(ErrorMessage = "Пожалуйста добавьте Тариф")]
        public string TariffName { get; set; }
    }
}
