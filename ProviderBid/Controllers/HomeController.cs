﻿using System.Web.Mvc;
using ProviderBid.Domain.Abstract;
using ProviderBid.Domain.Entities;

namespace ProviderBid.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBidRepository _repository;

        public HomeController(IBidRepository repo)
        {
            _repository = repo;
        }

        [HttpPost]
        public ActionResult Index(Bid bid)
        {
            if (ModelState.IsValid)
            {
                _repository.SaveBid(bid);
                return View("Completed");
            }
            else
            {
                return View(new Bid());
            }
        }

        public ViewResult Index()
        {
            return View(new Bid());
        }
    }
}
