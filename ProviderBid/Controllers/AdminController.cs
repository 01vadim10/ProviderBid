﻿using System;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using ProviderBid.Domain.Abstract;
using ProviderBid.Domain.Entities;

namespace ProviderBid.Controllers
{
    public class AdminController : Controller
    {
        private IBidRepository _repository;

        public AdminController(IBidRepository bidRepository)
        {
            _repository = bidRepository;
        }

        public ViewResult Index(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.IdSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.NameSortParm = sortOrder == "Name" ? "name_desc" : "Name";
            ViewBag.TariffSortParm = sortOrder == "Tariff" ? "tariff_desc" : "Tariff";

            var bids = from s in _repository.Bids
                       select s;

            switch (sortOrder)
            {
                case "id_desc":
                    bids = bids.OrderByDescending(s => s.BidID);
                    break;
                case "Name":
                    bids = bids.OrderBy(s => s.LastName);
                    break;
                case "name_desc":
                    bids = bids.OrderByDescending(s => s.LastName);
                    break;
                case "Tariff":
                    bids = bids.OrderBy(s => s.TariffName);
                    break;
                case "tariff_desc":
                    bids = bids.OrderByDescending(s => s.TariffName);
                    break;
                default:
                    bids = bids.OrderBy(s => s.BidID);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(bids.ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Edit(int bidId)
        {
            Bid bid = _repository.Bids.FirstOrDefault(p => p.BidID == bidId);
            return View(bid);
        }

        [HttpPost]
        public ActionResult Edit(Bid bid)
        {
            if (ModelState.IsValid)
            {
                _repository.SaveBid(bid);
                TempData["message"] = string.Format("{0} {1} был сохранен", bid.FirstName, bid.LastName);
                return RedirectToAction("Index");
            }
            else
            {
                return View(bid);
            }
        }

        public ActionResult Delete(int id)
        {
            Bid deletedBid = _repository.DeleteBid(id);
            if (deletedBid != null)
            {
                TempData["message"] = string.Format("{0} {1} был удален", deletedBid.FirstName, deletedBid.LastName);
            }
            return RedirectToAction("Index");
        }
    }
}
