﻿SET IDENTITY_INSERT [dbo].[Tariff] ON
INSERT INTO [dbo].[Tariff] ([TariffID], [Name]) VALUES (1, N'Light')
INSERT INTO [dbo].[Tariff] ([TariffID], [Name]) VALUES (2, N'Medium')
INSERT INTO [dbo].[Tariff] ([TariffID], [Name]) VALUES (3, N'Fast and Heavy')
INSERT INTO [dbo].[Tariff] ([TariffID], [Name]) VALUES (4, N'Infinity')
SET IDENTITY_INSERT [dbo].[Tariff] OFF
