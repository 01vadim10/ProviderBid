﻿CREATE TABLE [dbo].[Bids] (
    [BidID]      INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]  VARCHAR (100) NOT NULL,
    [LastName]   VARCHAR (100) NOT NULL,
    [MidName]    VARCHAR (100) NOT NULL,
    [Address]    VARCHAR (100) NOT NULL,
    [TariffName] VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([BidID] ASC)
);

